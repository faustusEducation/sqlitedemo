package faust.providence.app.sqlitedemo

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseForDemo(context: Context, name: String, factory: SQLiteDatabase.CursorFactory?, version: Int)
    : SQLiteOpenHelper(context, name, factory, version) {

    override fun onCreate(dataBase: SQLiteDatabase?) {
        dataBase?.execSQL(Participants.CREATE_TABLE)
    }


    /**
     * Wthen the VERSION number increased, drop the table and create a new one
     */
    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        if(p0 != null) {
            val tableName = Participants.TABLE_NAME
            val sql = "drop table if exists $tableName"
            p0.execSQL(sql)
            onCreate(p0)
        }
    }

    /**
     * static properties and functions
     */
    companion object {
        val DATABASE_NAME = "participants.db"
        // increase version to run onUpgrade
        val VERSION = 20

        /**
         * This function is kept unchanged mostly
         */
        fun get(context: Context): SQLiteDatabase {
            return DatabaseForDemo(context, DATABASE_NAME, null, VERSION).writableDatabase
        }
    }
}