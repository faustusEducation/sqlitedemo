package faust.providence.app.sqlitedemo

import android.content.Context

class Tools {
    companion object {
        /**
         * get the resource id from string value
         *
         * call by function:
         *      getDrawableiD(getResourceID("image_ev1"));
         * @param resName resource name (filename), means "@drawable/resName"
         */
        public fun getDrawable(ctx: Context, resName: String): Int {
            val resourceID = ctx.resources.getIdentifier(
                resName, "drawable",
                ctx.applicationInfo.packageName
            )
            // If unfound, resourceID is 0
            return resourceID
        }

    }
}