package faust.providence.app.sqlitedemo

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_edit.view.*
import kotlinx.android.synthetic.main.dbrow.view.*

class MyAdapter(context: Context,
               private val resource: Int,
               private val items: MutableList<Participants.Item>)
    : ArrayAdapter<Participants.Item>(context, resource, items) {

    override fun getView(position: Int,
                         convertView: View?,
                         parent: ViewGroup
    ): View {
        val itemView: LinearLayout
        // 讀取目前位置的記事物件
        val item = getItem(position)

        if (convertView == null) {
            // 建立項目畫面元件
            itemView = LinearLayout(context)
            val inflater = Context.LAYOUT_INFLATER_SERVICE
            val li = context.getSystemService(inflater) as LayoutInflater
            li.inflate(resource, itemView, true)
        } else {
            itemView = convertView as LinearLayout
        }

        itemView.txtTitle.text = item?.name + " ("+item?.alias+") ["+item?.group+"]"
        itemView.txtDescription.text = item?.remark
        if(item != null) {
            val id = Tools.getDrawable(context.applicationContext, item.img)
            if(id>0) {
                itemView.rowImage.setImageResource(id)
            } else {
                itemView.rowImage.setImageResource(R.mipmap.ic_launcher)
            }
        }

        return itemView
    }

    // 設定指定編號的記事資料
    operator fun set(index: Int, item: Participants.Item) {
        if (index >= 0 && index < items.size) {
            items[index] = item
            notifyDataSetChanged()
        }
    }

    // 讀取指定編號的記事資料
    operator fun get(index: Int): Participants.Item {
        return items[index]
    }

}
