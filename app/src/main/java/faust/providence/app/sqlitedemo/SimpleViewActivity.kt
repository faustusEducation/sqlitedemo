package faust.providence.app.sqlitedemo

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import faust.providence.app.sqlitedemo.R.id.all
import kotlinx.android.synthetic.main.activity_simple_view.*

class SimpleViewActivity : AppCompatActivity() {

    private val participants: Participants by lazy { Participants(applicationContext) }
    //private val participants = Participants(applicationContext)
    private val EDIT_ACTIVITY = 1
    private var groupId: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_simple_view)

        var items: ArrayList<Participants.Item>

        groupId = (if(intent.hasExtra("group")) this.intent.extras.getInt("group", -1) else -1)

        items = ( if(groupId<0) participants.all else participants.group(groupId))

        if(intent.hasExtra("view") && this.intent.extras.getString("view").equals("custom")) {
            val adapter = MyAdapter(this,R.layout.dbrow,items)
            this.listView.adapter = adapter
        } else {
            val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
            this.listView.adapter = adapter
        }

        this.listView.setOnItemClickListener() { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->
            val intent = Intent(this,EditActivity::class.java)
            intent.putExtra("id",items[i].id)
//            startActivity(intent)   // this line for calling without return value
            startActivityForResult(intent,EDIT_ACTIVITY)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == EDIT_ACTIVITY &&  resultCode == Activity.RESULT_OK && data != null ) {
            if(data.hasExtra("reflash")) {
                val reflash = data.extras.getBoolean("reflash", false)
                if (reflash) {
                    val items = ( if(groupId<0) participants.all else participants.group(groupId))
                    val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
                    this.listView.adapter = adapter
                }
            }
        }
    }
}
