package faust.providence.app.sqlitedemo

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

/**
 * table Praticipants
 *
 * CRUD = record (create/insert, read/retrieve, update, delete)
 */
class Participants(context: Context) {

    companion object {
        val TABLE_NAME = "participants"
        val ID = "_id"

        /**
         * Define the field names, note that, group is a preserved word
         */
        val NAME = "name"
        val ALIAS = "alias"
        val IMG = "img"
        val ATTENDANCE = "attendance"
        val GROUP = "groupid"
        val ROLE  = "role"
        val REMARK = "remark"
        val SCORE = "score"

        /**
         * Create table by SQL command
         *
         * There are three types of field in SQLite: text, integer and real, note that group is a reserved word
         */
        val CREATE_TABLE =
                "create table $TABLE_NAME  (" +
                "$ID integer primary key autoincrement," +
                "$NAME text not null," +
                "$ALIAS text not null,"  +
                "$IMG text not null," +
                "$ATTENDANCE integer not null," +
                "$GROUP integer not null," +
                "$ROLE text not null," +
                "$REMARK text not null," +
                "$SCORE real not null)"
    } // end of companion object

    /**
     * Definition of every item
     */
    class Item(name:String, alias:String, img:String, attendance:Int, group: Int, role: String, remark: String, score:Float) {
        var id: Long = 0
        var name: String
        var alias: String
        var img: String
        var attendance: Int
        var group: Int
        var role: String
        var remark: String
        var score: Float

        /**
         * default constructor
         */
        init {
            this.name = name
            this.alias = alias
            this.img = img
            this.attendance = attendance
            this.group = group
            this.role = role
            this.remark = remark
            this.score = score
        }

        /**
         * constructor for the minimum parameters
         */
        constructor(name:String,group:Int):this(name,name,"",1,group,"","",4.0f) {
        }

        /**
         * create the object from database cursor
         */
        constructor(cursor: Cursor)
                : this(cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getInt(4),
            cursor.getInt(5),cursor.getString(6),cursor.getString(7),cursor.getFloat(8)) {
            this.id = cursor.getLong(0)
        }

        /**
         * used for table insert, follow the definition of CREATE_TABLE
         */
        val contentValues: ContentValues
            get() {
                val cv = ContentValues()
                cv.put(Participants.NAME, this.name)
                cv.put(Participants.ALIAS, this.alias)
                cv.put(Participants.IMG, this.img)
                cv.put(Participants.ATTENDANCE,this.attendance)
                cv.put(Participants.GROUP, this.group)
                cv.put(Participants.ROLE, this.role)
                cv.put(Participants.REMARK, this.remark)
                cv.put(Participants.SCORE, this.score)
                return cv
            }

        override fun toString(): String {
            return this.name + " (" + this.alias +") <"+ this.group +"> " + this.role + (if (this.img.isEmpty()) "" else " ***")
        }
    } // end of definition of Item


    private val db: SQLiteDatabase = DatabaseForDemo.get(context)

    /**
     * get all records, if none, get empty list。ex:
     * val participants = Participants()
     * val allInfo = participant.all
     */
    val all: ArrayList<Item>
        get() {
            val result = ArrayList<Item>()
            val cursor = db.query(TABLE_NAME, null, null,
                null, null, null, null)
            while (cursor.moveToNext()) {
                result.add(Item(cursor))
            }
            return result
        }

    /**
     * get the top record
     */
    val top: Item?
        get() {
            val sql = "select * from $TABLE_NAME limit 1"
            val cursor = db.rawQuery(sql,null)
            val result = (if (cursor.moveToNext()) Item(cursor) else null)
            cursor.close()
            return result
        }
    /**
     * get how many records in the table
     */
    val count: Int
        get() {
            val cursor = db.rawQuery("select count(*) from $TABLE_NAME", null)
            var result = if(cursor.moveToNext()) cursor.getInt(0) else 0
            cursor.close()
            return result
        }
    /**
     * close the dabasebase
     */
    fun close() {
        db.close()
    }

    /**
     * create / insert a new record
     *
     * @param item  record to be inserted
     * @return the record inserted with id updated
     */
    fun insert(item: Item): Item {
        item.id = db.insert(TABLE_NAME, null, item.contentValues)
        return item
    }

    /**
     * read record from id
     */
    fun read(id: Long): Item? {
        var item: Item? = null
        val where = Participants.ID + "=" + id  //ex: "id=4"
        val result = this.db.query(TABLE_NAME,null,where,null,
            null,null,null,null)
        if(result.moveToNext()) {
            item = Item(result)
        }
        result.close()
        return item
    }

    /**
     * update the record by id
     *
     * @param item  item to be updated
     * @return true for record been updated
     */
    fun update(item: Item): Boolean {
        /**
         * use where = Participants.ID+"="+item.id.toString() is also correct, but use whereArgs will have more better performance
         */
        val where = Participants.ID + "=?"   // ex: "id=?"
        /**
         * use whereArgs = Array<String>(1, {id.toString()}), is also OK
         */
        val whereArgs = arrayOf<String>(item.id.toString())  //ex: arrayOf("4")
        return this.db.update(TABLE_NAME, item.contentValues, where, whereArgs) > 0
    }

    /**
     * delete record by id, sample to use whereArgs
     */
    fun delete(id: Long):Int {
        /**
         * use where = "id="+id.toString() is also correct, but use whereArgs will have more better performance
         */
        val where = Participants.ID + "=?"
        /**
         * use whereArgs = Array<String>(1, {id.toString()}), is also OK
         */
        val whereArgs = arrayOf<String>(id.toString())
        return this.db.delete(TABLE_NAME, where, whereArgs)
    }

    /**
     * polymorphism is means, same function name but different arguments
     */
    fun delete(item: Item):Int {
        return this.delete(item.id)
    }

    /**
     * get the items with the same group
     */
    fun group(group: Int):ArrayList<Item> {
        val result = ArrayList<Item>()
        val where = "$GROUP=?"
        val whereArgs = arrayOf<String>(group.toString())
        val cursor = this.db.query(TABLE_NAME,null,where,whereArgs,null,null,null,null)
        while(cursor.moveToNext()) {
            result.add(Item(cursor))
        }
        cursor.close()
        return result

    }
    /**
     * create the samples for testing
     */
    fun createSamples() {
        // For the file name, no extension (such as: .jpg) is required, and the filename must be lowercased, a-z, 0-9, can'b be Chinese
        val samples = arrayOf<Item>(
            Item("鄧志成","Faust","faust",1,0,"facilitater","",4.0f),
            Item("楚留香","Handsome","handsome",0,0,"sample","Just for scrolling",0f),
            Item("段玉","lucky","lucky",0,0,"sample","Just for scrolling",0f),
            Item("其他",0)
            )

        samples.forEach { item -> this.insert(item) }
    }

}