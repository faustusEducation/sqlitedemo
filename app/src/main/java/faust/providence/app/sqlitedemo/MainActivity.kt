package faust.providence.app.sqlitedemo

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val participants: Participants by lazy { Participants(applicationContext)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(participants.count == 0) {
            participants.createSamples()
        }

        val total = participants.count

        this.btnSimpleView.setOnClickListener {
            val intent = Intent(this,SimpleViewActivity::class.java)
            startActivity(intent)
        }

        this.btnGroup1.setOnClickListener {
            val intent = Intent(this,SimpleViewActivity::class.java)
            intent.putExtra("group",1)
            startActivity(intent)
        }

        this.btnGroup2.setOnClickListener {
            val intent = Intent(this,SimpleViewActivity::class.java)
            intent.putExtra("group",2)
            startActivity(intent)
        }

        this.btnCustomView.setOnClickListener {
            val intent = Intent(this, SimpleViewActivity::class.java)
            intent.putExtra("view", "custom")
            startActivity(intent)
        }
    }

    /**
     * function to demo TOAST
     */
    fun clickToast(view: View) {
        Toast.makeText(this@MainActivity, "This is a toast with short delay!", Toast.LENGTH_SHORT).show()
    }

    /**
     * function to demo dialog with more than one option
     */
    fun clickConfirm(view: View) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Cancel or Confirm?")
            builder.setMessage("just for demo")
            builder.setPositiveButton("Confirm"){ dialog, whichButton ->
                /**
                 * the message will be shown in the 4:Run window as I/System.out: ...
                 */
                println("** confirm is pressed **")
            }
            builder.setNegativeButton("Cancel") { dialog, whichButton ->
                /**
                 * the message will be shown in the 4:Run window as I/System.out: ...
                 */
                println("** cancel is pressed **")
            }
            val dialog = builder.create()
            dialog.show()
    }

    /**
     * function to demo ok dialog
     */
    fun clickOk(view:View) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Press OK to continue.")
        builder.setMessage("just for demo")
        builder.setPositiveButton("OK",null)
        val dialog = builder.create()
        dialog.show()
    }

}
