package faust.providence.app.sqlitedemo

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Gravity
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.activity_edit.*

class EditActivity : AppCompatActivity() {

    var id: Long = 0
    var participant: Participants.Item? = null
    val textColorView = Color.BLACK
    val textColorEdit = Color.BLUE
//    1.The following line is OK, and get instance in onCreate(...) **
//      such as: this.participants = Participants(this)
//    private var participants:Participants? = null

//    2.The following line is ERROR, it is must to use "lazy..."
//    private var participants = Participants(applicationContext)

//    3.The following line is OK too, create instance automatically
    private val participants: Participants by lazy { Participants(applicationContext)}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

//        if the instance is not created in the declartion, create here **
//        this.participants = Participants(this)

        id = this.intent.extras.getLong("id")

        this.participant = participants?.read(id)
        if (this.participant != null) {
            this.edtName.setText(this.participant?.name)
            this.edtAlias.setText(this.participant?.alias)
            this.edtGroup.setText(this.participant?.group.toString())
            this.edtRole.setText(this.participant?.role)
            this.edtScore.setText(this.participant?.score.toString())
            this.edtRemark.setText(this.participant?.remark)
            this.edtName.setTextColor(textColorView)
            this.edtAlias.setTextColor(textColorView)
            this.edtGroup.setTextColor(textColorView)
            this.edtRole.setTextColor(textColorView)
            this.edtScore.setTextColor(textColorView)
            this.edtRemark.setTextColor(textColorView)
        }

        this.edtName.isEnabled = false
        this.edtAlias.isEnabled = false
        this.edtGroup.isEnabled = false
        this.edtRole.isEnabled = false
        this.edtScore.isEnabled = false
        this.edtRemark.isEnabled = false

        this.edtGroup.gravity = Gravity.CENTER
        this.edtScore.gravity = Gravity.CENTER

        val drawableID = Tools.getDrawable(this.applicationContext, this.participant!!.img)
        if(drawableID > 0) {
            this.imageIcon.setImageResource(drawableID)
        }

        this.edtRemark.setOnEditorActionListener {
                view, i, keyEvent ->
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
            true
        }

        this.edtName.setOnEditorActionListener {
                view, i, keyEvent ->
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
                true
        }

        this.edtAlias.setOnEditorActionListener {
                view, i, keyEvent ->
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
            true
        }

        this.edtGroup.setOnEditorActionListener {
                view, i, keyEvent ->
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
            true
        }

        this.edtRole.setOnEditorActionListener {
                view, i, keyEvent ->
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
            true
        }

        this.edtScore.setOnEditorActionListener {
                view, i, keyEvent ->
            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            view.clearFocus()
            true
        }

    }

    /**
     * if edit, go saving, if in view mode, go editing
     *
     * @param view button view
     */
    fun onEditSave(view: View) {
        if(this.edtName.isEnabled) {
            this.participant?.name = this.edtName.text.toString()
            this.participant?.alias = this.edtAlias.text.toString()
            this.participant?.role = this.edtRole.text.toString()
            this.participant?.score = this.edtScore.text.toString().toFloat()
            this.participant?.remark = this.edtRemark.text.toString()
            this.participant?.group = this.edtGroup.text.toString().toInt()
            this.participants?.update(this.participant!!)

            this.backToParientActivity(true)
        } else {
            this.edtName.isEnabled = true
            this.edtAlias.isEnabled = true
            this.edtGroup.isEnabled = true
            this.edtRole.isEnabled = true
            this.edtScore.isEnabled = true
            this.edtRemark.isEnabled = true
            this.edtName.setTextColor(textColorEdit)
            this.edtAlias.setTextColor(textColorEdit)
            this.edtGroup.setTextColor(textColorEdit)
            this.edtRole.setTextColor(textColorEdit)
            this.edtScore.setTextColor(textColorEdit)
            this.edtRemark.setTextColor(textColorEdit)

            this.btnEditSave.text = "Save"
        }
    }

    /**
     * button: Quit onClick, must be public, can't be private or protected
     */
    fun onQuit(view: View) {
        if(this.edtName.isEnabled) {
            this.clickConfirmToQuit(view)
        } else {
            this.backToParientActivity()
        }
    }

    /**
     * function to demo dialog with more than one option
     */
    private fun clickConfirmToQuit(view: View) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Do you want to quit without save?")
        builder.setMessage("Please make sure and confirm.")
        builder.setPositiveButton("Confirm"){ dialog, whichButton -> this.backToParientActivity() }
        builder.setNegativeButton("Cancel") { dialog, whichButton -> {} }  // do nothing for cancel
        val dialog = builder.create()
        dialog.show()
    }

    private fun backToParientActivity(reflash: Boolean = false) {
        if(reflash) {
            intent = Intent()
            intent.putExtra("reflash", reflash)
            this.setResult(Activity.RESULT_OK, intent)
        }
        this.finish()
    }
}
